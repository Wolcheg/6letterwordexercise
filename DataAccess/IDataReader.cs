﻿using System.Collections.Generic;

namespace DataAccess {
  public interface IDataReader {
    HashSet<string>[] GetData(int maxLength);
  }
}
