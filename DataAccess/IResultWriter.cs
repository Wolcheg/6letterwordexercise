﻿using System.Collections.Generic;

namespace DataAccess {
  public interface IResultWriter {
    void Write(List<List<string>> result);
  }
}
