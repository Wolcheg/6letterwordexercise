﻿using System.Collections.Generic;
using System.IO;

namespace DataAccess {
  public class FileReader : IDataReader {
    private readonly string _fileName;

    public FileReader(string fileName) {
      _fileName = fileName;
    }

    public HashSet<string>[] GetData(int maxLength) {
      var data = new HashSet<string>[maxLength];
      for (int i = 0; i < maxLength; i++) {
        data[i] = new HashSet<string>();
      }

      var file = new StreamReader(_fileName);
      string line;
      while ((line = file.ReadLine()) != null) {
        var value = line.Trim();
        if (value.Length < maxLength + 1 && value.Length > 0) {
          data[value.Length - 1].Add(value);
        }
      }

      file.Close();

      return data;
    }
  }
}
