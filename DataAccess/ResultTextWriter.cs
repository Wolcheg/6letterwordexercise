﻿using System.Collections.Generic;
using System.IO;

namespace DataAccess {
  public class ResultTextWriter : IResultWriter {
    private readonly TextWriter _stream;

    public ResultTextWriter(TextWriter stream) {
      _stream = stream;
    }

    public void Write(List<List<string>> result) {
      _stream.WriteLine();
      foreach (var item in result) {
        foreach (var str in item) {
          _stream.WriteLine(str);
        }

        _stream.WriteLine();
      }
    }
  }
}
