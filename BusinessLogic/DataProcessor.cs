﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic {
  public class DataProcessor : IDataProcessor {
    public List<List<string>> ProcessData(HashSet<string>[] data, int maxLength) {

      // TODO: create validator, validate that HashSetArray cell number is equal to item's length in corresponding HashSet (off-by-1, of course)
      if (data.Length != maxLength) {
        throw new ArgumentException("Length of HashSetArray is supposed to be the same with maxLength", nameof(maxLength));
      }

      var result = new List<List<string>>();

      // so, any letter can be constructed as itself only
      var cache = data[0].ToDictionary(k => k, v => new List<string> { v });

      // go up and build every combination that is given in HashSets
      for (int l = 1; l < maxLength; l++) {

        foreach (var value in data[l]) {
          var combinations = new List<string> { value }; // anything can be constructed from itself

          // anything shorter we already can build, create combinations
          for (int i = 1; i <= l; i++) { 
            var left = value.Substring(0, i); 
            var right = value.Substring(i);

            if (cache.TryGetValue(left, out var leftCombinations)
                && cache.TryGetValue(right, out var rightCombinations)) {
              combinations.AddRange(MultiplyLists(leftCombinations, rightCombinations)); // create all combinations of left and right part
            }
          }

          cache.Add(value, combinations.Distinct().ToList());
        }
      }

      // build result
      var words = data[maxLength - 1];
      foreach (var word in words) {
        if (cache.TryGetValue(word, out var combinations)) {
          var values = combinations
            .Where(x => x.Length > word.Length) // remove trivial solutions e.g. foobar=foobar
            .Select(x => $"{x}={word}")
            .ToList();

          if (values.Any()) {
            result.Add(values);
          }
        }
      }

      return result;
    }

    private List<string> MultiplyLists(List<string> left, List<string> right) {

      return left.SelectMany(x => right, (x, y) => $"{x}+{y}").Distinct().ToList();

    }
  }
}
