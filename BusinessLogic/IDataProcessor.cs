﻿using System.Collections.Generic;

namespace BusinessLogic {
  public interface IDataProcessor {
    List<List<string>> ProcessData(HashSet<string>[] data, int maxLength);
  }
}
