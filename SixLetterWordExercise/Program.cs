﻿using System;
using BusinessLogic;
using DataAccess;
using Microsoft.Extensions.DependencyInjection;

namespace SixLetterWordExercise {
  class Program {
    // TODO: move to configuration
    private const int MaximumWordLength = 6;

    // relative for executive ..\SixLetterWordExercise\bin\Debug\netcoreapp3.1\SixLetterWordExercise.exe
    private const string FilePath = @"..\..\..\..\input.txt";

    static void Main(string[] args) {

      var serviceProvider = new ServiceCollection()
        .AddScoped<IDataReader, FileReader>(r => new FileReader(FilePath))
        .AddScoped<IDataProcessor, DataProcessor>()
        .AddScoped<IResultWriter, ResultTextWriter>(w => new ResultTextWriter(Console.Out))
        .BuildServiceProvider();

      Console.WriteLine("start application");

      var reader = serviceProvider.GetService<IDataReader>();
      var data = reader.GetData(MaximumWordLength); // don't read everything that is longer than we need

      var processor = serviceProvider.GetService<IDataProcessor>();
      var result = processor.ProcessData(data, MaximumWordLength);

      var resultWriter = serviceProvider.GetService<IResultWriter>();
      resultWriter.Write(result);

      Console.WriteLine("all done!");
      Console.ReadKey();
    }
  }
}
