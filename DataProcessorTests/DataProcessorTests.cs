﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic;
using Xunit;

namespace DataProcessorTests {
  public class DataProcessorTests {

    private readonly DataProcessor _processor;
    private readonly int _maxLength;

    public DataProcessorTests() {
      _processor = new DataProcessor();
      _maxLength = 6;
    }

    [Fact]
    public void WhenDataLength_IsDifferentFromMaxLength_Processor_ThrowArgumentException() {
      // Arrange
      var data = new HashSet<string>[10];

      // Act, Assert
      Assert.Throws<ArgumentException>(() => _processor.ProcessData(data, _maxLength));
    }

    [Fact]
    public void WhenData_DoesNotHaveCombinations_Processor_ReturnsEmptyList() {
      // Arrange
      var data = new HashSet<string>[]
      {
        new HashSet<string> {"o", "f", "b", "a"},
        new HashSet<string> {"oo", "fo", "bo"},
        new HashSet<string> {"var", "foo", "oba"},
        new HashSet<string> {"vary", "foob", "brar"},
        new HashSet<string> {"differ", "fooba", "fobar"},
        new HashSet<string> {"foobar"},
      };

      // Act
      var result = _processor.ProcessData(data, _maxLength);

      // Assert
      Assert.Empty(result);
    }

    public static IEnumerable<object[]> TwoPartsTestData = new List<object[]> {
      // contains only parts
      new object[] {
        new HashSet<string>[] {
          new HashSet<string> {"f", "r"},
          new HashSet<string> {"fo", "ar"},
          new HashSet<string> {"foo", "bar"},
          new HashSet<string> {"foob", "obar"},
          new HashSet<string> {"fooba", "oobar"},
          new HashSet<string> {"foobar"},
        },
        new List<string> {
          "f+oobar=foobar",
          "fooba+r=foobar",
          "fo+obar=foobar",
          "foob+ar=foobar",
          "foo+bar=foobar",
        }
      },
      // contains parts and garbage
      new object[] {
        new HashSet<string>[] {
          new HashSet<string> {"f", "z", "d", "r"},
          new HashSet<string> {"fo", "ar", "dz", "fa", "dr"},
          new HashSet<string> {"foo", "bar", "bra", "der"},
          new HashSet<string> {"foob", "zbar", "obar", "dert"},
          new HashSet<string> {"fooba", "zobar", "foobz", "oobar"},
          new HashSet<string> {"foobar"},
        },
        new List<string> {
          "f+oobar=foobar",
          "fooba+r=foobar",
          "fo+obar=foobar",
          "foob+ar=foobar",
          "foo+bar=foobar",
        }
      },
    };

    [Theory]
    [MemberData(nameof(TwoPartsTestData))]
    public void WhenData_HasTwoPartsCombinations_Processor_ReturnsList(HashSet<string>[] data, List<string> expected) {
      // Act
      var result = _processor.ProcessData(data, _maxLength).Single();

      // Assert
      Assert.Equal(result.Count, expected.Count);
      expected.ForEach(
          e => Assert.Contains(e, result)
      );
    }

    public static IEnumerable<object[]> MultiPartsTestData = new List<object[]> {
      new object[] {
        new HashSet<string>[] {
          new HashSet<string> {"f", "o", "b", "a", "r"},
          new HashSet<string> {"fo", "ob", "ar"},
          new HashSet<string> { },
          new HashSet<string> {"foob", "obar"},
          new HashSet<string> {"fooba", "oobar"},
          new HashSet<string> {"foobar"},
        },
        new List<string> {
          "f+oobar=foobar",
          "f+o+obar=foobar",
          "f+o+ob+ar=foobar",
          "f+o+ob+a+r=foobar",
          "f+o+o+b+ar=foobar",
          "f+o+o+b+a+r=foobar",
          "fo+obar=foobar",
          "fo+ob+ar=foobar",
          "fo+ob+a+r=foobar",
          "fo+o+b+ar=foobar",
          "fo+o+b+a+r=foobar",
          "foob+ar=foobar",
          "foob+a+r=foobar",
          "fooba+r=foobar",
        }
      }
    };

    [Theory]
    [MemberData(nameof(MultiPartsTestData))]
    public void WhenData_HasMultiplePartsCombinations_Processor_ReturnsList(HashSet<string>[] data, List<string> expected) {
      // Act
      var result = _processor.ProcessData(data, _maxLength).Single();

      // Assert
      Assert.Equal(result.Count, expected.Count);
      expected.ForEach(
        e => Assert.Contains(e, result)
      );
    }
  }
}
